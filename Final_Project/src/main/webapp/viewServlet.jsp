<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %><%--
  Created by IntelliJ IDEA.
  User: Ryan
  Date: 7/17/2021
  Time: 5:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
</head>
<body>

<br><br><br><br><br><br>
<table width="700px" align="center"
       style="border:1px solid #000000;">
    <tr>
        <td colspan=8 align="center"
            style="background-color:azure">
            <b>Employee Record</b></td>
    </tr>
    <tr style="background-color:darkseagreen">
        <td><b>Bicycle ID</b></td>
        <td><b>Brand</b></td>
        <td><b>Model</b></td>
        <td><b>Size</b></td>
        <td><b>Color</b></td>


    </tr>

    <%
        int count=0;
        String color = "#F9EBB3";


        if(request.getAttribute("bikeList")!=null)
        {
            ArrayList al = (ArrayList)request.getAttribute("bikeList");
            Iterator itr = al.iterator();


            while(itr.hasNext()){

                if((count%2)==0){
                    color = "#eeffee";
                }
                else{
                    color = "#F9EBB3";
                }
                count++;
                ArrayList bikeList = (ArrayList)itr.next();
    %>
    <tr style="background-color:<%=color%>;">
        <td><%=bikeList.get(0)%></td>
        <td><%=bikeList.get(1)%></td>
        <td><%=bikeList.get(3)%></td>
        <td><%=bikeList.get(4)%></td>
        <td><%=bikeList.get(5)%></td>
        <td><%=bikeList.get(6)%></td>
        <td><%=bikeList.get(7)%></td>
        <td><%=bikeList.get(8)%></td>
    </tr>
    <%
            }
        }
    %>
    <%
        if(count==0){
    %>
    <tr>
        <td colspan=8 align="center"
            style="background-color:darkgrey"><b>No Record</b></td>
    </tr>
    <%
        }
    %>
</table>
</body>
</html>

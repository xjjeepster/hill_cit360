package hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.Iterator;
import java.util.List;

public class testHibernate {

    SessionFactory factory = null;
    Session session = null;

    private static testHibernate single_instance = null;

    private testHibernate() { factory = hibernateUtils.getSessionFactory();}

    public static testHibernate getInstance() {
        if (single_instance == null) {
            single_instance = new testHibernate();
        }

        return single_instance;
    }


   public List<bike> getBikes() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from hibernate.bike";
            List<bike> cs = (List<bike>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }





}

package hibernate;

import javax.persistence.*;

@Entity
@Table(name = "bicycle_table")

public class bike {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "idbicycle_table")
    private int Id;

    @Column(name = "brand")
    private String Brand;

    @Column(name = "model")
    private String Model;

    @Column(name = "size")
    private String Size;

    @Column(name = "color")
    private String Color;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public String toString() {
        return Integer.toString(Id) + " " + Brand + " " + Model + " " + Size + " " + Color;
    }

}

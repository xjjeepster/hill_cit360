package com.example.Final_Project;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet", value = "/Servlet")
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");

        String size = null;

        int Height = Integer.parseInt(request.getParameter("Height"));

        if(Height >= 77) {
            out.println("<p>You should ride a XLarge or XXLarge Frame depending on inseam.</>");
        } else if (Height >= 74) {
            out.println("<p>You should ride a XLarge or Large Frame depending on inseam.</>");
        } else if (Height >= 71) {
            out.println("<p>You should ride a Large or MLarge Frame depending on inseam.</>");
        }else if (Height >= 68) {
            out.println("<p>You should ride a MLarge or Medium Frame depending on inseam.</>");
        }else if (Height >= 65) {
            out.println("<p>You should ride a Medium Frame.</>");
        }else if (Height >= 60) {
            out.println("<p>You should ride a Small Frame.</>");
        }else if (Height >= 54) {
            out.println("<p>You should ride a XSmall Frame.</>");
        }else if (Height < 54) {
            out.println("<p>You should ride a Youth Frame.</>");
        }
        out.println("</body></html>");

       /*Connection conn = null;
        String url = "jdbc:mysql://localhost:3306/";
        String driver = "com.mysql.jdbc.Driver";
        String dbName = "production";
        String userName = "root";
        String password = "Hudson2015!";*/
        /*Statement st;

        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url+dbName, userName, password);
            System.out.println("Connected to the database");
            String  brand  = request.getParameter("brand");
            String  model  = request.getParameter("model");
            String  size  = request.getParameter("size");
            String  color  = request.getParameter("color");

            ArrayList al = null;
            ArrayList bike_list = new ArrayList();
            String query =
                    "select * from bicycle_table";
            System.out.println("query " + query);
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(query);


            while(rs.next()){
                al  = new ArrayList();

                al.add(rs.getString(1));
                al.add(rs.getString(2));
                al.add(rs.getString(3));
                al.add(rs.getString(4));
                al.add(rs.getString(5));
                al.add(rs.getString(6));
                al.add(rs.getString(7));
                al.add(rs.getString(8));
                al.add(rs.getString(10));
                System.out.println("al :: "+al);
                bike_list.add(al);
            }

            request.setAttribute("bikeList",bike_list);

            //System.out.println("bikeList " + bike_list);

            // out.println("emp_list " + emp_list);

            //String nextJSP = "/viewServlet.jsp";
            //RequestDispatcher dispatcher =
            //        getServletContext().getRequestDispatcher(nextJSP);
            //dispatcher.forward(request,response);

            RequestDispatcher view = request.getRequestDispatcher("/viewServlet.jsp");
            view.forward(request, response);

            conn.close();
            System.out.println("Disconnected from database");
        } catch (Exception e) {
            e.printStackTrace();
        }*/



    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");
    }
}
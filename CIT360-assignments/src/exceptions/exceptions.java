package exceptions;

import java.util.Scanner;

public class exceptions {
    public static void main(String[] args) {
        /*  - Create a program that gathers input of two numbers from the keyboard in its main method and then calls the
                method below to calculate.
            - Write a method that divides the two numbers together (num1/num2) and returns the result.
                The method should include a throws clause for the most specific exception possible if the user enters
                zero for num2.
            - In your main method, use exception handling to catch the most specific exception possible.
                Display a descriptive message in the catch to tell the user what the problem is. Give the user a chance
                to retry the data entry. Display a message in the final statement.
            - Run and show the output when there is no exception and when a zero in the denominator is used.
            - Rewrite the data collection so that it uses data validation to ensure an error does not occur. If the
                user does type a zero in the denominator, display a message and give the user a chance to retry.
            - Run and show the output when there is no problem with data entry and when a zero is used in the
                denominator.*/

        // Tells User what program does
        System.out.println("\nThis program will divide two numbers entered by you!\n");

        // Requests first number from user.
        System.out.println("Enter numerator!");
        Scanner num = new Scanner(System.in);
        double numerator = num.nextDouble();


       /* // Request Second number from user (no exceptions)
        System.out.println("Enter Denominator!");
        Scanner dem = new Scanner(System.in);
        double denominator = dem.nextDouble();*/


        // Request second number from user. (exception added)
        System.out.println("Enter Denominator!");
        Scanner dem = new Scanner(System.in);
        double denominator;
        while (true) {
            denominator = dem.nextDouble();
            if (denominator == 0) {
                System.out.println("Not a valid #, Please enter a non-zero number:");
                continue;
            }
            break;
        }


        if (denominator == 0) {
            throw new IllegalArgumentException("0 can not be a denominator");
        } else {
            double answer = numerator / denominator;
            System.out.println(numerator + " divided by " + denominator + " equals " + answer);
            System.out.println("\nGoodbye!");
        }







    }
}

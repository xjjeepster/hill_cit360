package collections;

import java.util.*;
import java.util.Comparator;

import static java.util.Comparator.*;

public class collections {

    public static void main(String[] args) {

        /*  - Write a program that incorporates a list, a queue, a set, and a tree.
            - Your program should showcase the unique features of these different types of collections.
            - Your program should demonstrate you know how to use generics with your collections.
            - Implement the Comparator interface and show how to sort one of your collections.
         */

        // LIST Example

        System.out.println("\nList Example\n");

        List<String> days = new ArrayList<>();
        days.add("Monday");
        days.add("Tuesday");
        days.add("Wednesday");
        days.add("Thursday");
        days.add("Friday");
        days.add("Saturday");
        days.add("Sunday");

        for (String day : days) {
            System.out.println(day);
        }

        // Comparator Interface Example.  I decided to use reversOrder.
        System.out.println("\nComparator Interface Sorting in reverse alphabetical Order\n");
        days.sort(Comparator.reverseOrder());
        for (String day : days) {
            System.out.println(day);
        }


        System.out.println("\n----------------");
        // Queue Example

        System.out.println("\nQueue Example\n");
        Queue<String> drinkFavorites = new PriorityQueue<>();
        drinkFavorites.add("Mtn. Dew");
        drinkFavorites.add("Water");
        drinkFavorites.add("Lemonade");
        drinkFavorites.add("Water");
        drinkFavorites.add("Sprite");
        drinkFavorites.add("Lemonade");
        drinkFavorites.add("Rootbeer");

        Iterator<String> sodas = drinkFavorites.iterator();
        while (sodas.hasNext())
            System.out.println(drinkFavorites.poll());
        System.out.println("\n----------------");
        // Set Example

        System.out.println("\nSet Example\n");
        HashSet<String> foodFavorites = new HashSet<>();
        foodFavorites.add("Pizza");
        foodFavorites.add("Fettuccine Alfredo");
        foodFavorites.add("Steak");
        foodFavorites.add("Mango");
        foodFavorites.add("Hamburgers");
        foodFavorites.add("Rice Bowl");
        foodFavorites.add("McDonalds");
        foodFavorites.add("Pizza"); // Will Remove Duplicates
        foodFavorites.add("Steak"); // Will Remove Duplicates


        for (String food : foodFavorites) {
            System.out.println(food);
        }
        System.out.println("\n----------------");
        // Tree Example

        System.out.println("\nTree Example\n");

        Set<String> foodFaves = new TreeSet<>();
        foodFaves.add("Pizza");
        foodFaves.add("Fettuccine Alfredo");
        foodFaves.add("Steak");
        foodFaves.add("Mango");
        foodFaves.add("Hamburgers");
        foodFaves.add("Rice Bowl");
        foodFaves.add("McDonalds");
        foodFaves.add("Pizza"); // Will Remove Duplicates
        foodFaves.add("Steak"); // Will Remove Duplicates

        for (String food : foodFaves) {
            System.out.println(food);
        }

        System.out.println("\n----------------");

        // Using Generics to create a Dinner Menu List

        System.out.println("This is my family's favorites and day they get to eat it.\n");
        List<DinnerMenu> menu = new LinkedList<>();
        menu.add(new DinnerMenu("Ryan", "Pizza", "Mtn. Dew", days.get(3)));
        menu.add(new DinnerMenu("Val", "Fettuccine Alfredo", "Water", days.get(6)));
        menu.add(new DinnerMenu("Jeffrey", "Steak", "Lemonade", days.get(2)));
        menu.add(new DinnerMenu("Dusty", "Mango", "Water", days.get(5)));
        menu.add(new DinnerMenu("Kaden", "Hamburgers", "Sprite", days.get(0)));
        menu.add(new DinnerMenu("Suzie", "Rice Bowls", "Lemonade", days.get(4)));
        menu.add(new DinnerMenu("Hudson", "McDonalds", "Rootbeer", days.get(1)));

        for (DinnerMenu dm : menu) {
            System.out.println(dm);
        }

    }


}

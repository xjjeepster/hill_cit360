package collections;

public class DinnerMenu{

    private String name;
    private String food;
    private String drink;
    private String day;

    public DinnerMenu(String name, String food, String drink, String day) {
        this.name = name;
        this.food = food;
        this.drink = drink;
        this.day = day;
    }


    public String toString() {
        return "Name: " + name + ", Favorite Food: " + food + ", Favorite Drink: " + drink + ", Day to Eat: " + day;
    }

}




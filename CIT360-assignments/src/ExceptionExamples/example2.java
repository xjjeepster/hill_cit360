package ExceptionExamples;

import java.util.Scanner;

public class example2 {
    public static void main(String[] args) {
        System.out.println("\nThis program will divide two numbers entered by you!\n");

        System.out.println("Enter numerator!");
        Scanner num = new Scanner(System.in);
        double numerator = num.nextDouble();

        System.out.println("Enter Denominator!");
        Scanner dem = new Scanner(System.in);
        double denominator;
        while (true) {
            denominator = dem.nextDouble();
            if (denominator == 0) {
                System.out.println("Not a valid #, Please enter a non-zero number:");
                continue;
            }
            break;
        }



            double answer = numerator / denominator;
            System.out.println(numerator + " divided by " + denominator + " equals " + answer);
            System.out.println("\nGoodbye!");

    }

}

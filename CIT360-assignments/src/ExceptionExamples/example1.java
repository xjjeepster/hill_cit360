package ExceptionExamples;

import java.util.Scanner;

public class example1 {
    public static void main(String[] args) {
        System.out.println("Enter Positive Number");
        Scanner num = new Scanner(System.in);
        double number = num.nextDouble();

        if (number <= 0) {
            throw new IllegalArgumentException("Must be a positive Number");
        } else {
            System.out.println("Congratulations");
        }

    }
}

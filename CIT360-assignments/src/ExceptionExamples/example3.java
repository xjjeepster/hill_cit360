package ExceptionExamples;
import java.util.Scanner;
public class example3 {
    public static void main(String[] args) {
        System.out.println("Given today's day of the week and some number of days in the future this program will tell you the day of the week for the future day.");
        System.out.println("Enter today's numerical value for day of the week: 0 = Sunday, 1 = Monday, etc :");
        Scanner today = new Scanner(System.in);
        int Today = today.nextInt();
        if (Today > 6 || Today < 0) {
            System.out.println("Input invalid must enter value 0 - 6!");
            System.exit(1);
        }

        System.out.println("Enter the number of days in the future.");
        Scanner fDay = new Scanner(System.in);
        int FutureDay = fDay.nextInt();
        if (FutureDay < 0) {
            System.out.println("You must enter a positive Number!");
            System.exit(54);
        }

        int FutureDate = (Today + FutureDay) % 7;
        String dayOfWeek = switch (Today) {
            case 0 -> "Sunday";
            case 1 -> "Monday";
            case 2 -> "Tuesday";
            case 3 -> "Wednesday";
            case 4 -> "Thursday";
            case 5 -> "Friday";
            case 6 -> "Saturday";
            default -> "";
        };

        if (FutureDate == 0) {
            System.out.printf("Today is %s and the future day is Sunday. ", dayOfWeek);
        } else if (FutureDate == 1) {
            System.out.printf("Today is %s and the future day is Monday. ", dayOfWeek);
        } else if (FutureDate == 2) {
            System.out.printf("Today is %s and the future day is Tuesday. ", dayOfWeek);
        } else if (FutureDate == 3) {
            System.out.printf("Today is %s and the future day is Wednesday. ", dayOfWeek);
        } else if (FutureDate == 4) {
            System.out.printf("Today is %s and the future day is Thursday. ", dayOfWeek);
        } else if (FutureDate == 5) {
            System.out.printf("Today is %s and the future day is Friday. ", dayOfWeek);
        } else if (FutureDate == 6) {
            System.out.printf("Today is %s and the future day is Saturday. ", dayOfWeek);
        }

        System.out.println("Goodbye.");
    }
}

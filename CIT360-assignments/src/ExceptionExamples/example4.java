package ExceptionExamples;
import java.util.Scanner;
public class example4 {
    public static void main(String[] args) {
        // Tell User What Program Does

        System.out.println("This Program converts a hexadecimal digit into a four digit binary number.");

        // Prompt User to enter hexadecimal digit

        System.out.println("Enter a single hexadecimal digit:");
        Scanner hexNumber = new Scanner(System.in);
        char ch =  hexNumber.next().charAt(0);


        //Convert hexadecimal digit into a four digit binary number. If not a valid hexadecimal tell the user
        // and terminate the program by using a return, or System.exit

        String hexDec = switch (ch) {
            case '0' -> "0000";
            case '1' -> "0001";
            case '2' -> "0010";
            case '3' -> "0011";
            case '4' -> "0100";
            case '5' -> "0101";
            case '6' -> "0110";
            case '7' -> "0111";
            case '8' -> "1000";
            case '9' -> "1001";
            case 'A', 'a' -> "1010";
            case 'B', 'b' -> "1011";
            case 'C', 'c' -> "1100";
            case 'D', 'd' -> "1101";
            case 'E', 'e' -> "1110";
            case 'F', 'f' -> "1111";
            default -> throw new IllegalArgumentException(ch + " is not a valid hexadecimal digit!");
        };
        System.out.println("The binary value is " + hexDec + ".");

        //Output a goodbye message.
        System.out.println("Goodbye.");
    }
}

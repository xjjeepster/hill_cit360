import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BumperTest {

    @Test
    void getBumperBrand() {
        Bumper b = new Bumper("OEM", 315);
        assertEquals("OEM", b.getBumperBrand());
    }

    @Test
    void getPriceWholeDollar() {
        Bumper b = new Bumper("OEM", 315);
        assertNotNull(b.getPriceWholeDollar());

    }


    @Test
    void setBumperBrand() {
        Bumper b = new Bumper("OEM", 315);
        Bumper c = new Bumper("OEM", 315);
        c.setBumperBrand("Rough Country");
        assertNotSame(b,c);
    }

    @Test
    void setPriceWholeDollar() {
        Bumper b = new Bumper("OEM", 315);
        b.setPriceWholeDollar(900);
        assertTrue(900 <= 1500);
    }


}
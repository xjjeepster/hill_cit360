public class Bumper {

    private String bumperBrand;
    private double priceWholeDollar;

    public Bumper(String bumperBrand, double priceWholeDollar) {
        this.bumperBrand = bumperBrand;
        this.priceWholeDollar = priceWholeDollar;
    }

    public String getBumperBrand() {
        return bumperBrand;
    }

    public double getPriceWholeDollar() {
        return priceWholeDollar;
    }

    public void setBumperBrand(String bumperBrand) {
        this.bumperBrand = bumperBrand;
    }

    public void setPriceWholeDollar(double priceWholeDollar) {
        this.priceWholeDollar = priceWholeDollar;
    }

}

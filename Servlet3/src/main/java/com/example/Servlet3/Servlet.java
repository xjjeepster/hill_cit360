package com.example.Servlet3;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet", value = "/Servlet")
public class Servlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        out.println("<style>");
        out.println("h1 {");
        out.println("color:blue;");
        out.println("background-color:gray;");
        out.println("border: 1px solid black;");
        out.println("</style>");
        out.println("<style>");
        out.println("p {");
        out.println("</style>");
        String car = request.getParameter("car");
        String color = request.getParameter("color");
        out.println("<h1>   Worlds Best Car</h1>");
        out.println("<p>    The " + color +" " + car + " is a good car.</p>");
        out.println("<p>    The worlds best car is the one that gets you to your destination safely.  </p>");
        out.println("</body></html>");
        }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");
    }
}

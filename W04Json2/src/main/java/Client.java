import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.*;
import java.io.*;
import java.util.*;


public class Client{

    public static void main(String[] args) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            Dog dog = mapper.readValue(new File("C:\\Users\\Ryan\\Documents\\git\\hill_cit360\\W04Json2\\target\\dog.json"), Dog.class);
            System.out.println(dog);

            String dog3 = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(dog);

            System.out.println(dog3);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

// Dog Object

public class Dog {
    private String breed;
    private String color;
    private String owner;
    private String license;

    public Dog() {
    }

    public Dog(String breed, String color, String owner, String license) {
        this.breed = breed;
        this.color = color;
        this.owner = owner;
        this.license = license;
    }

    public String getBreed() {
        return this.breed;
    }

    public String getColor() {
        return this.color;
    }

    public String getOwner() {
        return this.owner;
    }

    public String getLicense() {
        return this.license;
    }

    public String toSring() {
        return "Breed:  " + this.breed + " Color: " + this.color + " Owner: " + this.owner + " License: " + this.license;
    }
}
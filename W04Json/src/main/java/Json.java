import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.File;


public class Json {

   public static String dogToJson(Dog dog) {
        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(dog);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }

    public static Dog JSONToDog(String s) {
        ObjectMapper mapper = new ObjectMapper();
        Dog dog = null;

        try {
            dog = mapper.readValue(s, Dog.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return dog;
    }

    public static void main(String[] args) {
        Dog dg = new Dog(
                "Beagle",
                "Tri-Color",
                "Ryan Hill",
                "N001"
        );

        String json = Json.dogToJson(dg);
        System.out.println(json);

        Dog dog2 = Json.JSONToDog(json);
        System.out.println(dog2);

    }

   /* public static void main(String[] args) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();

            Dog dog = new Dog(
                    "Beagle",
                    "Tri-Color",
                    "Ryan Hill",
                    "N001"
            );

            objectMapper.writeValue(new File("target/dog.json"), dog);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }*/
}

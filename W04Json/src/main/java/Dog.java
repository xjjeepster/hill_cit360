public class Dog {

    private String breed;
    private String color;
    private String owner;
    private String license;

    public Dog() {
        super();
    }

    public Dog(String breed, String color, String owner, String license) {
        this.breed = breed;
        this.color = color;
        this.owner = owner;
        this.license = license;
    }

    public String getBreed() {
        return breed;
    }

    public String getColor() {
        return color;
    }

    public String getOwner() {
        return owner;
    }

    public String getLicense() {
        return license;
    }
    public String toSring() {
        return "Breed:  " + breed + " Color: " + color + " Owner: " + owner + " License: " + license;
    }
}

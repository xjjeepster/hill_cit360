package hibernatetest;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.management.Query;
import java.util.Date;
import java.util.List;


public class testHibernate {

    SessionFactory factory = null;
    Session session = null;

    private static testHibernate single_instance = null;

    private testHibernate() {
        factory = HibernateUtils.getSessionFactory();
    }

    public static testHibernate getInstance() {
        if (single_instance == null) {
            single_instance = new testHibernate();
        }

        return single_instance;
    }

    public List<actor> getActors() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from hibernatetest.actor";
            List<actor> cs = (List<actor>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }


    public actor getActor(int id) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from hibernatetest.actor where id=" + Integer.toString(id);
            actor a = (actor) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return a;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
    public actor updateName() {
        try {
            session = factory.openSession();
            session.getTransaction();
            Transaction tx = session.beginTransaction();

            actor Actor = session.load(actor.class, 199);
            Actor.setFirstName("RUMPELSTILTSKIN");
            Actor.setLastName("CORONA");
            session.update(Actor);
            tx.commit();
            return Actor;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

   /*public List<actor> getFirstName() {
        try {
            session = factory.openSession();
            session.getTransaction().begin();

            session.getTransaction().commit();
            return fn;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();

        }
    }*/


}

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadMain {
    public static void main(String[] args) throws InterruptedException{

        ExecutorService myservice = Executors.newFixedThreadPool(3);

        myservice.execute(new AThread());
        myservice.execute(new BThread());
        myservice.execute(new CThread());
        myservice.execute(new DThread());
        myservice.execute(new EThread());

        myservice.shutdown();


    }
}


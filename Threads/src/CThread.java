import java.util.Random;

public class CThread implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            Random random = new Random();
            int number = random.nextInt(1000);
            System.out.println("CThread's " + number);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

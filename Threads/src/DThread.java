import java.util.Random;

public class DThread implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            Random random = new Random();
            int number = random.nextInt(100);
            System.out.println("DThread's " + number);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

import java.util.Random;

public class EThread implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            Random random = new Random();
            int number = random.nextInt(10);
            System.out.println("EThread's " + number);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

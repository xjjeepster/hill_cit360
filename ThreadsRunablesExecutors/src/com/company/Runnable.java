package com.company;

@FunctionalInterface
public interface Runnable {
    public abstract void run();
}
